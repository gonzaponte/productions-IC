import sys
import argparse

import numpy  as np
import tables as tb
import pandas as pd

from utils import file_is_ok
from utils import FileType
from utils import compute_average_xy
from utils import compute_average_z
from utils import dump_df
from utils import dump_array


parser = argparse.ArgumentParser()
parser.add_argument("-i",  "--input-file", type=str, help= "Input file")
parser.add_argument("-o", "--output-file", type=str, help="Output file")

args            = parser.parse_args(sys.argv[1:])
filename_input  = args. input_file
filename_output = args.output_file


if __name__ == "__main__":
    energy    = []
    charge    = []
    x_mean    = []
    y_mean    = []
    z_mean    = []
    x_std     = []
    y_std     = []
    z_std     = []
    avwf_pmt  = None
    avwf_sipm = None

    if not file_is_ok(filename_input, FileType.MCRD):
        print("File is corrupt")
        sys.exit(1)

    with tb.open_file(filename_input) as file:
        pmtrd    = file.root. pmtrd
        sipmrd   = file.root.sipmrd
        evt_no   = file.root.Run.events[:, 0]
        run_no   = file.root.Run.runInfo[0][0]
        n_events = pmtrd.shape[0]

        for i in range(n_events):
            pmt_wfs  =  pmtrd[i]
            sipm_wfs = sipmrd[i]
            energy.append( pmt_wfs.sum())
            charge.append(sipm_wfs.sum())

            if avwf_pmt  is None: avwf_pmt  = np.zeros( pmtrd.shape[-1])
            if avwf_sipm is None: avwf_sipm = np.zeros(sipmrd.shape[-1])
            avwf_pmt  +=  pmt_wfs.sum(axis=0)
            avwf_sipm += sipm_wfs.sum(axis=0)

            x, y, xstd, ystd = compute_average_xy(sipm_wfs, run_no)
            z   , zstd       = compute_average_z (pmt_wfs)
            x_mean.append(x)
            y_mean.append(y)
            z_mean.append(z)
            x_std .append(xstd)
            y_std .append(ystd)
            z_std .append(zstd)

    energy     = np.array(energy)
    charge     = np.array(charge)
    x_mean     = np.array(x_mean)
    y_mean     = np.array(y_mean)
    z_mean     = np.array(z_mean) / 1e3 # in mm
    x_std      = np.array(x_std )
    y_std      = np.array(y_std )
    z_std      = np.array(z_std ) / 1e3 # in mm
    avwf_pmt  /= n_events
    avwf_sipm /= n_events

    data = pd.DataFrame(dict(evt_no = evt_no,
                             energy = energy,
                             charge = charge,
                             x_mean = x_mean,
                             y_mean = y_mean,
                             z_mean = z_mean,
                             x_std  = x_std ,
                             y_std  = y_std ,
                             z_std  = z_std ), index=evt_no)

    dump_df   (data     , filename_output, group="global", node="data")
    dump_array(avwf_pmt , filename_output, group="avwf"  , node="pmt" )
    dump_array(avwf_sipm, filename_output, group="avwf"  , node="sipm")
