import numpy  as np
import tables as tb

from enum        import Enum
from collections import namedtuple

from invisible_cities.database.load_db        import DataSiPM
from invisible_cities.core    .core_functions import weighted_mean_and_std
from invisible_cities.reco    .xy_algorithms  import corona


class FileType(Enum):
    MCRD = "MCRD"
    RWF  =  "RWF"
    BLR  =  "BLR"
    PMAP = "PMAP"
    KDST = "KDST"
    HDST = "HDST"


nevt_fields = namedtuple("nevt_fields", "all_same fields")


def file_type(string):
    try                  : return getattr(FileType, string.upper())
    except AttributeError: raise ValueError(f"File type {string} not supported")


def is_corrupt(filename):
    try                   : tb.open_file(filename).close(); return False
    except tb.HDF5ExtError:                                 return True


def get_number_of_events_rd(filename):
    with tb.open_file(filename) as file:
        pmtrd   = file.root. pmtrd
        sipmrd  = file.root.sipmrd
        return dict( pmtrd =  pmtrd.shape[0],
                    sipmrd = sipmrd.shape[0])


def get_number_of_events_run(filename):
    with tb.open_file(filename) as file:
        return dict(run = file.root.Run.events.nrows)


def get_number_of_events(filename, filetype):
    fields = {}

    if filetype is FileType.MCRD:
        fields.update(get_number_of_events_rd (filename))
        fields.update(get_number_of_events_run(filename))
    else:
        raise ValueError(f"File type {filetype} not supported")

    all_same = np.all(np.diff(list(fields.values())) == 0)
    return nevt_fields(all_same, fields)


def file_is_ok(filename, filetype):
    return not is_corrupt(filename) and get_number_of_events(filename, filetype).all_same


def compute_average_xy_wfs(wfs, run_number):
    sipm_qs   = wfs.sum(axis=1)
    if not np.any(sipm_qs > 0): return (201, 201, 0, 0)
    sipm_data = DataSiPM(run_number)
    sipm_pos  = np.stack([sipm_data.X.values, sipm_data.Y.values], axis=1)
    clusters  = corona(sipm_pos, sipm_qs, sipm_data, lm_radius=-1, Qthr=-1, msipm=0) # barycenter!
    b         = next(iter(clusters))
    return b.XY[0], b.XY[1], b.Xrms, b.Yrms


def compute_average_z_wfs(wfs, trigger_time = 100e3):
    sum_wf = wfs.sum(axis=0)
    if not np.any(sum_wf): return -1, -1

    time = np.arange(sum_wf.size).astype(np.double)
    t_mean, t_std  = weighted_mean_and_std(time, sum_wf) # ns
    delta_t = t_mean - trigger_time
    return delta_t, t_std


def dump_df(data, filename, group, node, mode="w"):
    data.to_hdf(filename,
                key     = group  , mode         = mode,
                format  = "table", data_columns = True,
                complib = "zlib" , complevel    = 4   )

    with tb.open_file(filename, "r+") as file:
        file.rename_node(getattr(file.root, group).table, node)


def dump_array(data, filename, group, node, mode="w"):
    with tb.open_file(filename, mode=mode) as file:
        if group not in filename.root: file.create_group(file.root, "avwf")
        group = getattr(file.root, group)
        
        file.create_array(group, node, data)
