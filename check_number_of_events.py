import sys
import argparse

import tables as tb

from utils import FileType
from utils import get_number_of_events


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-files", type=str           , help="input files"              , nargs="*")
parser.add_argument("-t", "--file-type"  , type=file_type     , help="type of data"             )
parser.add_argument("-n", "--n-events"   , type=int           , help="expected number of events")
parser.add_argument("-x", "--no-warnings", action="store_true", help="do not print warnings"    )


if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])

    for filename in args.input_files:
        all_same, fields = get_number_of_events(filename, parser.file_type)
        if not all_same:
            print(f"ERROR: File {filename} does not have a consistent structure:")
            for field, nevt in fields.items():
                print(f"{field} -> {nevt}")

        if args.no_warnings: continue
        for field, nevt in fields.items():
            if not nevt == args.n_events:
                print(f"WARNING: Unexpected number of events: {nevt} / {args.n_events}")
            break
